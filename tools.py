# -*- coding: utf-8 -*-

"""Действия не связанные с **ftrack**"""

import json
import tempfile
import os

import bpy

def refresh_proxy(context):
    """
    Пересоздаёт выделенный прокси,
    с сохранением положения и анимационного экшена.
    Копирует ``child_off`` констрейн на **root** контрол и на сам прокси объект(если есть).
    """
    #(get data)
    ob_data={}
    ob_child_ofs={}
    root_child_ofs={}
    objects=[]
    for ob in context.selected_objects:
        objects.append(ob.name)
    for name in objects:
        ob=bpy.data.objects[name]
        #(ob_data)
        ob_data[ob.name]={}
        if ob.proxy:
            ob_data[ob.name]["original"]=ob.proxy
        else:
            ob_data[ob.name]["original"]=None
        ob_data[ob.name]["loc"]=ob.location[:]
        ob_data[ob.name]["rot"]=ob.rotation_euler[:]
        ob_data[ob.name]["scl"]=ob.scale[:]
        if ob.animation_data:
            ob_data[ob.name]["action"]=ob.animation_data.action
        else:
            ob_data[ob.name]["action"]=None
        #(object CHILD_OF)
        ob_child_ofs[ob.name]=[]
        for constr in ob.constraints:
            if constr.type=="CHILD_OF":
                ob_child_ofs[ob.name].append({
                    "name":constr.name,
                    "target":constr.target.name,
                    "subtarget":constr.subtarget,
                    })
                ob.constraints.remove(constr)
        #(root bone CHILD_OF)
        ob.select_set(True)
        context.view_layer.objects.active = ob
        bpy.ops.object.mode_set(mode='POSE')
        pose_root=None
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
        if pose_root:
            root_child_ofs[ob.name]=[]
            for constr in pose_root.constraints:
                if constr.type=="CHILD_OF":
                    root_child_ofs[ob.name].append({
                        "name":constr.name,
                        "target":constr.target.name,
                        "subtarget":constr.subtarget,
                        })
                    pose_root.constraints.remove(constr)
    #(remove)
    for name in objects:
        ob=bpy.data.objects[name]
        if ob_data[name]["original"]:
            ob.select_set(True)
            context.view_layer.objects.active = ob
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.data.objects.remove(ob, do_unlink=True)

    tmpdir=tempfile.gettempdir()
    with open(os.path.join(tmpdir,'ob_data.json'), 'w') as f:
        json.dump(str(ob_data), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'ob_child_ofs.json'), 'w') as f:
        json.dump(str(ob_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'root_child_ofs.json'), 'w') as f:
        json.dump(str(root_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'objects.json'), 'w') as f:
        json.dump(str(objects), f, sort_keys=True, indent=4)
        
    #(create proxy)
    for name in objects:
        if "original" in ob_data[name]:
            original=ob_data[name]["original"]
            original.select_set(True)
            context.view_layer.objects.active = original
            #(create proxy)
            bpy.ops.object.proxy_make(object='DEFAULT')
            proxy=bpy.data.objects[f"{original.name}_proxy"]
            #(-- hide original)
            original.select_set(False)
            original.hide_set(True)
            #(-- activate proxy)
            proxy.select_set(True)
            context.view_layer.objects.active = proxy
            #(-- proxy position)
            proxy.location=ob_data[name]["loc"]
            proxy.rotation_euler=ob_data[name]["rot"]
            proxy.scale=ob_data[name]["scl"]
            #(-- action)
            if ob_data[name]["action"]:
                proxy.animation_data_create()
                proxy.animation_data.action=ob_data[name]["action"]

    #(create constraints)
    for name in objects:
        ob=bpy.data.objects[name]
        ob.select_set(True)
        context.view_layer.objects.active = ob
        #(-- pose constraint)
        bpy.ops.object.mode_set(mode='POSE')
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
            if name in root_child_ofs:
                for item in root_child_ofs[name]:
                    constr=pose_root.constraints.new("CHILD_OF")
                    constr.name=item["name"]
                    # try:
                    # constr.target=item["target"]
                    constr.target=bpy.data.objects[item["target"]]
                    constr.subtarget=item["subtarget"]
                    constr.influence=0
                    # except Exception as e:
                    #     print(e)
                    #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        #(-- ob constraint)
        bpy.ops.object.mode_set(mode='OBJECT')
        if name in ob_child_ofs:
            for item in ob_child_ofs[name]:
                constr=ob.constraints.new("CHILD_OF")
                constr.name=item["name"]
                # try:
                # constr.target=item["target"]
                constr.target=bpy.data.objects[item["target"]]
                constr.subtarget=item["subtarget"]
                constr.influence=0
                # except Exception as e:
                #     print(e)
                #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        
        return(True, "Ok!")