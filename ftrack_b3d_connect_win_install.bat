@echo off
echo Start of installation Ftrack_b3d_connect ...

set /p bversion=Input version of blender[2.93]:

if "%bversion%" == "" (
set bversion=2.93
)

echo version - %bversion%

set addons_dir=%appdata%\Blender Foundation\Blender\%bversion%\scripts\addons
set modules_dir=%appdata%\Blender Foundation\Blender\%bversion%\scripts\modules

echo %addons_dir%
echo %modules_dir%

if not exist "%addons_dir%" (mkdir "%addons_dir%")
if not exist "%modules_dir%" (mkdir "%modules_dir%")

cd "%addons_dir%"

git clone https://gitlab.com/volodya-renderberg/ftrack-b3d-connect.git
cd ftrack-b3d-connect\modules

powershell -command "Expand-Archive python_modules_win.zip ""%modules_dir%"""

echo installation is complete.

pause