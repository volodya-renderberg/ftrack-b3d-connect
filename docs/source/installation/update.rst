Update
======

Contents:

.. toctree::
   :maxdepth: 1

   update_windows
   update_linux
   update_macos