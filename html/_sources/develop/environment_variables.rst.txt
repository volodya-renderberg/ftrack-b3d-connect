.. _environment-variables-page:

Environment Variables
=====================


FTRACK_B3D_BACKUP_ROOT
----------------------

* Путь до директории переданной для загрузки данных задач с фтрек на локаль.


.. _FTRACK_B3D_ROLE:

FTRACK_B3D_ROLE
---------------

* Роль юзера: ``Working`` - исполнитель или ``Cheking`` - проверяющий.


.. _FTRACK_B3D_PROJECTS_DIR:

FTRACK_B3D_PROJECTS_DIR
-----------------------


* Путь до текущей директрии для размещения проектов, определяемая в :func:`settings.set_projects_folder`


.. _FTRACK_B3D_CURRENT_PROJECT_ID:

FTRACK_B3D_CURRENT_PROJECT_ID
-----------------------------

* ``id`` текущего проекта.


.. _FTRACK_B3D_CURRENT_PROJECT_NAME:

FTRACK_B3D_CURRENT_PROJECT_NAME
-------------------------------

* ``name`` текущего проекта.


.. FTRACK_B3D_CURRENT_PROJECT_FULLNAME:

FTRACK_B3D_CURRENT_PROJECT_FULLNAME
-----------------------------------

* ``full_name`` текущего проекта.


.. _FTRACK_B3D_CURRENT_PROJECT_FPS:

FTRACK_B3D_CURRENT_PROJECT_FPS
------------------------------

* ``fps`` текущего проекта.


.. _FTRACK_B3D_CURRENT_PROJECT_WIDTH:

FTRACK_B3D_CURRENT_PROJECT_WIDTH
--------------------------------

* ``width`` ширина в пикселах для разрешения текущего проекта.


.. _FTRACK_B3D_CURRENT_PROJECT_HEIGHT:

FTRACK_B3D_CURRENT_PROJECT_HEIGHT
---------------------------------

* ``height`` высота в пикселах для разрешения текущего проекта.


.. _FTRACK_B3D_CURRENT_PROJECT_FSTART:

FTRACK_B3D_CURRENT_PROJECT_FSTART
---------------------------------

* Кадр с которого будет начинаться анимация во всех шотах. По умолчанию 1000.


.. _FTRACK_B3D_CURRENT_TASK_ID:

FTRACK_B3D_CURRENT_TASK_ID
--------------------------

* ``id`` текущей задачи.


.. _FTRACK_B3D_CURRENT_ASSET_NAME:

FTRACK_B3D_CURRENT_ASSET_NAME
-----------------------------

* ``name`` имя ассета текущей задачи.


.. _FTRACK_B3D_CURRENT_ASSET_TYPE:

FTRACK_B3D_CURRENT_ASSET_TYPE
-----------------------------

* ``object_type.name`` имя типа ассета текущей задачи.


.. _FTRACK_B3D_CURRENT_ASSET_ID:

FTRACK_B3D_CURRENT_ASSET_ID
---------------------------

* ``id`` ассета текущей задачи.


.. _FTRACK_B3D_AUTH_USER:

FTRACK_B3D_AUTH_USER
--------------------

* ``username`` авторизованного пользователя.


.. _FTRACK_B3D_SCENE_FEND:

FTRACK_B3D_SCENE_FEND
---------------------

* финальный кадр шота, считается от нуля. Определяется только для тех задач у которых тип родителя ``Shot``.


.. _FTRACK_B3D_SHOTS_STATUSES:

FTRACK_B3D_SHOTS_STATUSES
-------------------------

* *json* словарь с ключами: ``time`` - время записи в isoformat(), ``data`` - словарь статусов по именам шотов. Это статусы задач последних версий ревью шотов. Заполняется в :func:`working.select_shot_sequences_by_task_status`