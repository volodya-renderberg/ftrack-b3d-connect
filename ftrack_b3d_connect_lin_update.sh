#!/bin/bash

echo Start of Ftrack_b3d_connect update ...

read -p "Input version of blender[2.93]:" bversion

if ! [ -n "$bversion" ]; then 
    bversion=2.93
fi
echo version - ${bversion}

addons_dir=~/.config/blender/${bversion}/scripts/addons

cd ${addons_dir}/ftrack-b3d-connect

git pull origin master

echo update is complete.