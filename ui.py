import webbrowser
import os
import logging
import tempfile

import bpy

from . import light_collections_managment as lcm
from . import settings
from . import path_utils
from . import working
from . import db
from . import server_connect as srvconn
from . import check
from . import hooks
from . import tools

logging.basicConfig(
    filename=os.path.join(tempfile.gettempdir(),'ftrack_log'),
    filemode='a',
    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
    datefmt='%H:%M:%S',
    level=logging.INFO,
    # level=logging.DEBUG
    )

MONTAGE_OBJECT_TYPES=(
    "Episode",
    "Sequence",
)

ANIMATION_OBJECT_TYPES=(
    "Shot",
    "Scene",
)

CAUTION_TO_INPROGRESS=['Открытие приведёт','к смене статуса на "IN PROGRESS"','всё равно продолжить?']
CAUTION_OF_LAG=['Локальная версия отстаёт',' от сервера на [%s] версий!']

class G():
    sess=None
    current_panel="task list"
    projects=tuple()
    projects_dict=None
    task_list=tuple()
    task_dict=None
    versions_list=tuple()
    ftrack_versions_list=tuple()
    ftrack_versions_dict=None
    components=()
    sourcepath=None
    graphics_editors={}
    role='Working'
    check=None
    selected_task=None
    task_templates=()
    message=tuple()# tuple: кортеж строк для label
    lag=0 # int: отставание локальнойверсии от версии на сервере. заполняется в "ftrack.select_task"
    incoming_tasks=dict() #словарь входящих задач для текущей задачи.заполняется в ftrack.open_scene_from_incoming_task

def fill_projects(self, context):
    if G.projects:
        return G.projects


def change_project(self, context):
    bpy.ops.ftrack.switch_panels(action="to task list")

def fill_tasks_lists(self, context):
    prj_id = context.scene.ftrack_projects
    prj=G.projects_dict[prj_id]
    # os.environ["FTRACK_B3D_ROLE"]=context.scene.ftrack_role
    os.environ["FTRACK_B3D_CURRENT_PROJECT_ID"]=prj_id
    os.environ["FTRACK_B3D_CURRENT_PROJECT_NAME"]=prj["name"]
    os.environ["FTRACK_B3D_CURRENT_PROJECT_FULLNAME"]=prj["full_name"]
    os.environ["FTRACK_B3D_CURRENT_PROJECT_FPS"]=str(G.projects_dict[prj_id]["custom_attributes"]["fps"])
    os.environ["FTRACK_B3D_CURRENT_PROJECT_WIDTH"]=str(G.projects_dict[prj_id]["custom_attributes"]["width"])
    os.environ["FTRACK_B3D_CURRENT_PROJECT_HEIGHT"]=str(G.projects_dict[prj_id]["custom_attributes"]["height"])
    if "fstart" in G.projects_dict[prj_id]["custom_attributes"]:
        os.environ["FTRACK_B3D_CURRENT_PROJECT_FSTART"]=str(G.projects_dict[prj_id]["custom_attributes"]["fstart"])
    else:
        os.environ["FTRACK_B3D_CURRENT_PROJECT_FSTART"]="1000"
    if G.role=='Cheking':
        r,m=srvconn.get_tasks_list(check_list=True)
    else:
        r,m=srvconn.get_tasks_list()
    if r:
        G.task_list=m[0]
        G.task_dict=m[1]
    context.scene.ftrack_panel_task_list=True

def change_role(self, context):
    os.environ["FTRACK_B3D_ROLE"]=G.role=context.scene.ftrack_role
    fill_tasks_lists(self, context)

def read_graphics_editors():
    G.graphics_editors=settings.read_graphics_editors()

def set_projects_folder(self, context):
    r, projects_dir = settings.set_projects_folder(self.ftrack_current_projects_dir)
    if r:
        self.ftrack_current_projects_dir = projects_dir

def get_versions_list(task_id=False):
    b,r = srvconn.get_versions_list(task_id=task_id)
    if not b:
        G.ftrack_versions_list=tuple()
        G.ftrack_versions_dict=dict()
        return(b,r)
    else:
        G.ftrack_versions_list=r[0]
        G.ftrack_versions_dict=r[1]
        return(True, "Ok")


def get_task_templates():
    b,r=srvconn.get_task_templates()
    if b:
        G.task_templates=r
    else:
        print(f"{'7'*50} - {r}")


def fill_task_templates(self, context):
    if G.task_templates:
        params=list()
        for t in G.task_templates:
            params.append((t["name"],)*3)
        print(f"{'8'*50} - {params} - {G.task_templates}")
        return params
    return []


def lag_test_of_version(self, task):
    """Проверка отставания версии и заполнение G.lag натуральным числом величины отставания. """
    b,r=working.lag_test_of_version(task)
    if not b:
        self.report({'WARNING'}, str(r))
    else:
        G.lag=r
        self.report({'INFO'}, str(r))


def set_params():
    # filter/search
    bpy.types.Scene.ftrack_filter_gen = bpy.props.StringProperty(name = 'Filter', default='', update = None)
    bpy.types.Scene.ftrack_filter_source = bpy.props.StringProperty(name = 'Filter', default='', update = None)
    # seting
    bpy.types.Scene.ftrack_setting_in_process = bpy.props.BoolProperty(name = 'Setting', default=False, update = None)
    projects_folder = settings.get_projects_folder()
    bpy.types.Scene.ftrack_current_projects_dir = bpy.props.StringProperty(name = 'Projects Dir', subtype='DIR_PATH', default=projects_folder, update = set_projects_folder)
    # auth
    bpy.types.Scene.ftrack_auth_in_process = bpy.props.BoolProperty(name = 'Auth', default=False, update = None)
    bpy.types.Scene.ftrack_auth_current_user = bpy.props.StringProperty(name = 'Auth user', default='No authorization', update = None)
    # bpy.types.Scene.ftrack_projects = bpy.props.EnumProperty(items=fill_projects, name = 'Projects', default=None, update = fill_tasks_lists)
    bpy.types.Scene.ftrack_projects = bpy.props.EnumProperty(items=fill_projects, name = 'Projects', default=None, update = change_project)
    bpy.types.Scene.ftrack_role = bpy.props.EnumProperty(items=[('Working',)*3,('Cheking',)*3], name = 'Role', default=None, update = change_role)
    # panels
    bpy.types.Scene.ftrack_panel_task_list = bpy.props.BoolProperty(name = 'Task list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_selected_task = bpy.props.BoolProperty(name = 'Selected task', default=False, update = None)
    bpy.types.Scene.ftrack_panel_working_task = bpy.props.BoolProperty(name = 'Working task', default=False, update = None)
    bpy.types.Scene.ftrack_panel_versions_list = bpy.props.BoolProperty(name = 'Versions list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_ftrack_versions_list = bpy.props.BoolProperty(name = 'Ftracl versions list', default=False, update = None)
    bpy.types.Scene.ftrack_panel_sources = bpy.props.BoolProperty(name = 'Source panel', default=False, update = None)
    # animatic tools
    # bpy.types.Scene.ftrack_task_templates = bpy.props.EnumProperty(items=fill_task_templates, name = 'Task templates', default=None, update = None)
    #
    auth_data=settings.read_auth_data()
    if auth_data:
        bpy.types.Scene.ftrack_server_url = bpy.props.StringProperty(name = 'Server URL', default=auth_data["server_url"], update = None)
        bpy.types.Scene.ftrack_api_key = bpy.props.StringProperty(name = 'Api Key', default=auth_data["api_key"], update = None)
        bpy.types.Scene.ftrack_api_user = bpy.props.StringProperty(name = 'Api User', default=auth_data["api_user"], update = None)
    else:
        bpy.types.Scene.ftrack_server_url = bpy.props.StringProperty(name = 'Server URL', default='https://mycompany.ftrackapp.com', update = None)
        bpy.types.Scene.ftrack_api_key = bpy.props.StringProperty(name = 'Api Key', default='', update = None)
        bpy.types.Scene.ftrack_api_user = bpy.props.StringProperty(name = 'Api User', default='', update = None)

    read_graphics_editors()


class FTRACK_light_panel(bpy.types.Panel):
    bl_idname = "FTRACK_PT_light_panel"
    bl_label = 'Light (Ftrack):'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'Light///'

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        col.label(text='All collections')
        col.operator("ftrack.light_local_save_collections")
        col.operator("ftrack.light_set_from_file_collections").action="local"
        col = layout.column(align = True)
        col.operator("ftrack.light_set_from_file_collections", text="Set from incoming Location").action="global"
        col = layout.column(align = True)
        #
        for col_name in lcm.COLLECTIONS:
            col.label(text=f'{col_name}:')
            row = col.row(align = True)
            row.operator("ftrack.light_select_objects_of_collections").collection=col_name
            row.operator("ftrack.light_add_selected_objects_to_collections").collection=col_name
            row.operator("ftrack.light_remove_selected_objects_from_collections").collection=col_name
        col.operator("ftrack.light_select_objects_of_collections", text="Select object no collections").collection="No"


class FTRACK_main_panel(bpy.types.Panel):
    bl_idname = "FTRACK_PT_main_panel"
    bl_label = 'Ftrack:'
    # bl_space_type = "VIEW_3D"
    # bl_region_type = "UI"
    bl_category = 'Ftrack'
    #bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column(align = True)
        
        row = col.row(align = True)
        row.label(text='')
        row.operator('ftrack.manual', icon='QUESTION')
        
        col.label(text=context.scene.ftrack_auth_current_user)


        ## SETTING
        if context.scene.ftrack_setting_in_process:
            col.label(text='PROJECTS FOLDER:')
            col.prop(context.scene, "ftrack_current_projects_dir")
            # row = col.row(align = True)
            
            col.label(text='GRAPHICS EDITORS:')
            if G.graphics_editors:
                for name in G.graphics_editors:
                    row = col.row(align = True)
                    row.label(text=name)
                    row.label(text=G.graphics_editors[name])
                    row.operator("ftrack.del_graphics_editors").alias=name
            col.operator("ftrack.add_graphics_editors")

            col = layout.column(align = True)
            col.operator("ftrack.setting_start", text='Cansel').action='cansel'
        ## AUTH
        elif context.scene.ftrack_auth_in_process:
            col = layout.column(align = True)
            col.prop(context.scene, "ftrack_server_url")
            col.prop(context.scene, "ftrack_api_user")
            col.prop(context.scene, "ftrack_api_key")
            row = col.row(align = True)
            row.operator("ftrack.authorization", text='Cansel').action='cansel'
            row.operator("ftrack.authorization").action='auth'
        else:
            row = col.row(align = True)
            row.operator("ftrack.auth_start")
            row.operator("ftrack.setting_start").action='start'
        
        ## PROJECTS LIST
        layout = self.layout
        col = layout.column(align = True)
        # col.separator_spacer()
        col.separator(factor=4.0)
        if G.projects:
            col.prop(context.scene, "ftrack_projects")

        ## TASK LIST
        if context.scene.ftrack_panel_task_list:
            col = layout.column(align = True)
            col.prop(context.scene, "ftrack_role")

            col.label(text='TASKS:')
            col.prop(context.scene, "ftrack_filter_gen")
            for task in G.task_list:
                conditions=(
                    context.scene.ftrack_filter_gen.lower() in task['parent']['name'].lower(),
                    context.scene.ftrack_filter_gen.lower() in task['name'].lower(),
                    context.scene.ftrack_filter_gen.lower() in task['status']['name'].lower()
                    )
                if context.scene.ftrack_filter_gen and not any(conditions):
                    continue
                else:
                    row = col.row(align = True)
                    row.label(text=f"[{task['parent']['name']}] {task['name']}")
                    row.label(text=task['status']['name'])
                    row.operator("ftrack.select_task").task_id=task["id"]

        ## SELECTED TASK
        if context.scene.ftrack_panel_selected_task:
            col = layout.column(align = True)
            G.selected_task=G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
            col.label(text=f'SELECTED TASK: [{G.selected_task["parent"]["name"]}] '
                f'{G.selected_task["name"]}')
            #(From Ftrack:)
            col.label(text="From Ftrack:")
            col.operator("ftrack.download_version").action="start"
            if G.selected_task["parent"]["object_type"]["name"] in hooks.FILM_OBJECTS:
                pass
            else:
                col.operator("ftrack.update_incoming")
            #(Local:)
            col.label(text="Local:")
            if G.role=='Working':
                col.operator("ftrack.open_scene_from_incoming_task").dialog=True
                col.operator("ftrack.current_scene_to_work")
                col.operator("ftrack.open_work_top_version")
                col.operator("ftrack.open_work_version").action="start"
            else:
                col.operator("ftrack.open_work_top_version", text="Look")
                col.operator("ftrack.open_work_version", text="Look version").action="start"
            col.operator("ftrack.open_file_browser")

            col = layout.column(align = True)
            col.operator("ftrack.switch_panels").action="to task list"

        ## WORKING OF TASK
        if context.scene.ftrack_panel_working_task:
            layout = self.layout
            сol = layout.column(align = True)
            col.label(text=f'{G.role.upper()} OF TASK: [{G.selected_task["parent"]["name"]}] '
                f'{G.selected_task["name"]}')
            #
            col.label(text="From Ftrack:")
            col.operator("ftrack.download_version").action="start"
            #(Sources:)
            col.label(text="Sources:")
            if G.selected_task["parent"]["object_type"]["name"] in hooks.FILM_OBJECTS:
                # col.operator("ftrack.download_animatic")
                pass
            else:
                col.operator("ftrack.update_incoming")
                col.operator("ftrack.sources_panel")
            #
            if G.role=='Working':
                col = layout.column(align = True)
                col.label(text="Local:")
                col.operator("ftrack.open_work_version").action="start"
                col.operator("ftrack.open_file_browser")

                #(NOTES)
                col.label(text="Notes:")
                b1=col.operator("ftrack.open_last_version_by_webbrowser", text="Open last Commit by web")
                b1.prefix="Commit_"
                b1.shot_name=""
                b2=col.operator("ftrack.open_last_version_by_webbrowser", text="Open last Review by web")
                b2.prefix="Review_"
                b2.shot_name=""
                #(Textures:)
                if G.selected_task["parent"]["object_type"]["name"] in hooks.FILM_OBJECTS:
                    pass
                else:
                    col.label(text="Textures:")
                    col.operator("ftrack.collecting_textures")
                    col.operator("ftrack.open_graphics_editor")

                #(Animation Tools:)
                if G.selected_task["parent"]["object_type"]["name"] in ANIMATION_OBJECT_TYPES:
                    col = layout.column(align = True)
                    col.label(text="Animation Tools:")
                    # col.operator("ftrack.pack_links", text="Append selected link").action="selected"
                    col.operator("ftrack.refresh_proxy", text="Refresh proxy")
                    col.operator("ftrack.download_animatic_to_shot")

                #(Animatic Tools:)
                if G.selected_task["parent"]["object_type"]["name"] in hooks.FILM_OBJECTS:
                    # layout = self.layout
                    box=layout.box()
                    box.label(text="Animatic Tools:")
                    box.operator("ftrack.download_animatic")
                    steps=box.box()
                    steps.label(text="Make Shots:")
                    row = steps.row(align = False)
                    row.label(text="step 1:")
                    row.operator("ftrack.rename_animatic_markers", text="Rename markers to Sequence").action="sequence"
                    row = steps.row(align = False)
                    row.label(text="step 2:")
                    row.operator("ftrack.rename_animatic_markers", text="Rename markers to Shot").action="shot"
                    row = steps.row(align = False)
                    row.label(text="step 3:")
                    row.operator("ftrack.create_shots_from_markers", text="Create Shots")
                    #
                    edit_shots=box.box()
                    edit_shots.label(text="Edit Shots:")
                    col=edit_shots.column(align = False)
                    col.operator("ftrack.re_create_selected_shots")
                    col.operator("ftrack.download_shot_animatic_to_episode")
                    #
                    review=box.box()
                    review.label(text="Review:")
                    col=review.column(align = False)
                    row = col.row(align = False)
                    row.label(text="Select shot sequences:")
                    row.operator("ftrack.select_shot_sequences_by_task_status", text="Сhoice status")
                    col.operator("ftrack.download_review")
                    #
                    col=box.column(align = False)
                    col.operator("ftrack.timing_from_selected_shots")
                    #
                    check_box=box.box()
                    shot_name=None
                    if bpy.context.selected_sequences:
                        if bpy.context.selected_sequences[0].name.startswith("Shot"):
                            shot_name=bpy.context.selected_sequences[0].name.split('.')[0]
                    check_box.label(text=f'Checking shot: {shot_name}')
                    col=check_box.column(align = False)
                    if shot_name:
                        b=col.operator("ftrack.open_last_version_by_webbrowser", text="Open last Review by web")
                        b.prefix="Review_"
                        b.shot_name=shot_name

                        col.label(text="Change status:")
                        b1=col.operator("ftrack.change_status_of_selected_shot", text='to "Rejected"')
                        b1.status_name="Rejected"
                        b1.shot_name=shot_name
                        b2=col.operator("ftrack.change_status_of_selected_shot", text='to "Needs attention"')
                        b2.status_name="Needs attention"
                        b2.shot_name=shot_name
                        b3=col.operator("ftrack.change_status_of_selected_shot", text='to "Proposed final"')
                        b3.status_name="Proposed final"
                        b3.shot_name=shot_name
                        b4=col.operator("ftrack.change_status_of_selected_shot", text='to "Approved"')
                        b4.status_name="Approved"
                        b4.shot_name=shot_name
                    
                #(To Ftrack:)
                col = layout.column(align = True)
                col.label(text="To Ftrack:")
                col.operator("ftrack.check_scene")
                col.operator("ftrack.commit", text="Commit")
                col.operator("ftrack.change_status").status_name="Pending Review"

                # if G.selected_task["parent"]["object_type"]["name"] in ANIMATION_OBJECT_TYPES:
                col.label(text="Playblast:")
                col.operator("ftrack.playblast")
                col.operator("ftrack.playblast_to_review")
            else:
                col = layout.column(align = True)
                col.label(text="Local:")
                col.operator("ftrack.open_work_version", text="Look version").action="start"
                col.operator("ftrack.check_scene")

                col.label(text="Notes:")
                b1=col.operator("ftrack.open_last_version_by_webbrowser", text="Open last Commit by web")
                b1.prefix="Commit_"
                b1.shot_name=""
                b2=col.operator("ftrack.open_last_version_by_webbrowser", text="Open last Review by web")
                b2.prefix="Review_"
                b2.shot_name=""
                
                col.label(text="Change status:")
                col.operator("ftrack.change_status", text='to "Rejected"').status_name="Rejected"
                col.operator("ftrack.change_status", text='to "Needs attention"').status_name="Needs attention"
                col.operator("ftrack.change_status", text='to "Proposed final"').status_name="Proposed final"
                col.operator("ftrack.change_status", text='to "Approved"').status_name="Approved"
            
            col = layout.column(align = True)
            col.operator("ftrack.switch_panels").action="to task list"

        ## SOURCE PANEL
        if context.scene.ftrack_panel_sources:
            layout = self.layout
            col = layout.column(align = True)
            col.label(text="Sources:")
            col.prop(context.scene, "ftrack_filter_source")
            for task, vers, byte, url, status in G.components[0]:
                conditions=(
                    context.scene.ftrack_filter_source.lower() in task['parent']['name'].lower(),
                    context.scene.ftrack_filter_source.lower() in task['name'].lower(),
                    )
                if context.scene.ftrack_filter_source and not any(conditions):
                    continue
                else:
                    row = col.row(align = True)
                    row.label(text=task['parent']['name'])
                    row.label(text=task['name'])
                    row.label(text=status)
                    if status=='missing':
                        row.label(text='')
                        row.operator("ftrack.reload_incoming_version").task_id=task["id"]
                    else:
                        row.operator("ftrack.link_of_source", text="Add").task_id=task["id"]
                        row.operator("ftrack.reload_incoming_version").task_id=task["id"]
            col.operator("ftrack.switch_panels").action=f"to working"

        ## LIST OF LOCAL VERSIONS
        if context.scene.ftrack_panel_versions_list:
            layout = self.layout
            col = layout.column(align = True)
            for version in G.versions_list:
                row = col.row(align = True)
                row.label(text=str(version['local_version']))
                # row.label(text=str(version['version_id']))
                row.label(text=str(version['created']))
                row.label(text=version['description'])
                row.operator("ftrack.open_work_version", text="Open").action=str(version['local_version'])
            col.operator("ftrack.switch_panels").action=f"to {G.current_panel}"

        ## LIST OF FTRACK VERSIONS
        if context.scene.ftrack_panel_ftrack_versions_list:
            col = layout.column(align = True)
            for version in G.ftrack_versions_list:
                try:
                    row = col.row(align = True)
                    row.label(text=version['link'][-1:][0]['name'])
                    row.label(text=f"[{version['user']['last_name']}]: {version['comment']}")
                    row.label(text=str(version['date'].naive))
                    row.operator("ftrack.download_version", text='download').action=version['id']
                except Exception as e:
                    print(e)
            col.operator("ftrack.switch_panels").action=f"to {G.current_panel}"

class FTRACK_VIEW_3D_panel(FTRACK_main_panel, bpy.types.Panel):
    bl_idname = "FTRACK_PT_VIEW_3D_panel"
    bl_label = 'Ftrack:'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'Ftrack'

class FTRACK_SEQUENCE_EDITOR_panel(FTRACK_main_panel, bpy.types.Panel):
    bl_idname = "FTRACK_PT_SEQUENCE_EDITOR_panel"
    bl_label = 'Ftrack:'
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "UI"
    bl_category = 'Ftrack'

class FTRACK_auth_start(bpy.types.Operator):
    bl_idname = "ftrack.auth_start"
    bl_label = "Authorization"

    def execute(self, context):
        context.scene.ftrack_auth_in_process=True
        return {'FINISHED'}


class FTRACK_setting_start(bpy.types.Operator):
    bl_idname = "ftrack.setting_start"
    bl_label = "Setting"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        if self.action=='cansel':
            context.scene.ftrack_setting_in_process=False
        else:
            context.scene.ftrack_setting_in_process=True
        return {'FINISHED'}

class FTRACK_authorization(bpy.types.Operator):
    bl_idname = "ftrack.authorization"
    bl_label = "Auth"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        if self.action=='cansel':
            context.scene.ftrack_auth_in_process=False
            return {'FINISHED'}
        r,m=auth(context)
        if not r:
            self.report({'WARNING'}, m)
        else:
            self.report({'INFO'}, m)
        return {'FINISHED'}

class FTRACK_manual(bpy.types.Operator):
    bl_idname = "ftrack.manual"
    bl_label = "Manual"
    
    def execute(self, context):
        url='https://ftrack-b3d-connect-manual.readthedocs.io/en/latest'
        # url=f'file:///{os.environ["FTRACK_DOCS_PATH"]}'
        webbrowser.open_new_tab(url)
        return{'FINISHED'}


class FTRACK_api(bpy.types.Operator):
    bl_idname = "ftrack.api"
    bl_label = "Python api"
    
    def execute(self, context):
        # url='https://ftrack-b3d-connect-manual.readthedocs.io/en/latest'
        url=f'file:///{os.environ["FTRACK_DOCS_PATH"]}'
        webbrowser.open_new_tab(url)
        return{'FINISHED'}


class FTRACK_set_folder(bpy.types.Operator):
    bl_idname = "ftrack.set_folder"
    bl_label = "Set Folder"
    
    directory:  bpy.props.StringProperty(subtype="DIR_PATH")
        
    def execute(self, context):
        result = settings.set_projects_folder(self.directory)
        if not result[0]:
            self.report({'WARNING'}, result[1])
        else:   
            self.report({'INFO'}, (f'set Projects folder: {result[1]}'))
        return{'FINISHED'}
    
    def invoke(self, context, event):
        wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class FTRACK_select_task(bpy.types.Operator):
    bl_idname = "ftrack.select_task"
    bl_label = "select"

    task_id:  bpy.props.StringProperty(name="task_id")
    
    def execute(self, context):
        # (1)
        task=G.task_dict[self.task_id]
        os.environ["FTRACK_B3D_CURRENT_TASK_ID"]=self.task_id
        os.environ["FTRACK_B3D_CURRENT_ASSET_NAME"]=task['parent']['name']
        os.environ["FTRACK_B3D_CURRENT_ASSET_TYPE"]=task['parent']['object_type']['name']
        os.environ["FTRACK_B3D_CURRENT_ASSET_ID"]=task['parent']['id']
        if task['parent']['object_type']['name']=="Shot":
            os.environ["FTRACK_B3D_SCENE_FEND"]=str(float(os.environ["FTRACK_B3D_CURRENT_PROJECT_FSTART"]) + task['parent']['custom_attributes']['fend'] - task['parent']['custom_attributes']['fstart'])
        lag_test_of_version(self, G.task_dict[self.task_id])
        self.report({'INFO'}, self.task_id)
        # (2)
        bpy.ops.ftrack.switch_panels(action="to selected")
        return{'FINISHED'}

class FTRACK_switch_panels(bpy.types.Operator):
    bl_idname = "ftrack.switch_panels"
    bl_label = "Close"

    action: bpy.props.StringProperty(name="action")

    def execute(self, context):
        self.report({'INFO'}, self.action)
        # (1)
        if self.action=="to task list":
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_task_list=True
            G.current_panel="task list"
            fill_tasks_lists(self, context)
            srvconn.timer_stop()
        elif self.action=="to versions":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_versions_list=True
        elif self.action=="to ftrack versions":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_ftrack_versions_list=True
        elif self.action=="to working":
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_working_task=True
            G.current_panel="working"
        elif self.action=="to selected":
            context.scene.ftrack_panel_sources=False
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_selected_task=True
            G.current_panel="selected"
        elif self.action=="to sources":
            context.scene.ftrack_panel_versions_list=False
            context.scene.ftrack_panel_ftrack_versions_list=False
            context.scene.ftrack_panel_task_list=False
            context.scene.ftrack_panel_selected_task=False
            context.scene.ftrack_panel_working_task=False
            context.scene.ftrack_panel_sources=True
        return{'FINISHED'}

class FTRACK_current_scene_to_work(bpy.types.Operator):
    bl_idname = "ftrack.current_scene_to_work"
    bl_label = "Current scene to work"

    def execute(self, context):
        # (0)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        # (1)
        b,r = working.check_file_structure(task)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (2)
        b,r=working.save_current_scene_as_top_version(task)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (3)
        bpy.ops.ftrack.switch_panels(action="to working")
        # (end)
        self.report({'INFO'}, "Current scene to work")
        return{'FINISHED'}


class FTRACK_open_scene_from_incoming_task(bpy.types.Operator):
    bl_idname = "ftrack.open_scene_from_incoming_task"
    bl_label = "Open from incoming"

    dialog: bpy.props.BoolProperty(default=False)
    task_id: bpy.props.StringProperty(default='')

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]

        # (2)
        row=G.incoming_tasks.get(self.task_id)
        if row:
            incoming_task=row['task']
        else:
            self.report({'INFO'}, "No incoming task!")
            return{'FINISHED'}
        b,r=working.open_scene_from_incoming_task(task, incoming_task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="Incoming tasks:")
        for task_id in G.incoming_tasks:
            row = col.row(align = True)
            row.label(text=G.incoming_tasks[task_id]['task']['name'])
            row.label(text=G.incoming_tasks[task_id]['status'])
            op=row.operator('ftrack.open_scene_from_incoming_task', text="Open")
            op.task_id=task_id
            op.dialog=False

    def invoke(self, context, event):
        if self.dialog:
            b,r=srvconn.get_incoming_tasks(G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]])
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            G.incoming_tasks=r
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

        
class FTRACK_open_work_top_version(bpy.types.Operator):
    bl_idname = "ftrack.open_work_top_version"
    bl_label = "Open"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        # (2)
        if G.role=="Working":
            b,r=working.open(task)
        else:
            b,r=working.open(task, look=True)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        # (3)
        bpy.ops.ftrack.switch_panels(action="to working")
        # (end)
        self.report({'INFO'}, "Open")
        return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        for line in G.message:
            col.label(text=line)

    def invoke(self, context, event):
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        G.message=list()
        dialog=False
        if G.lag:
            for item in CAUTION_OF_LAG:
                try:
                    G.message.append(item % G.lag)
                except:
                    G.message.append(item)
            dialog=True
        if task['status']['name'] in srvconn.CHEKING_STATUSES and G.role=="Working":
            G.message.append('-------')
            G.message=G.message+CAUTION_TO_INPROGRESS
            dialog=True

        if dialog:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)
        
class FTRACK_open_work_version(bpy.types.Operator):
    bl_idname = "ftrack.open_work_version"
    bl_label = "Open version"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        # (2)
        if self.action=='start':
            # (1)
            b,r = db.get_all_work_versions(task)
            if not b:
                self.report({'WARNING'}, v)
                return{'FINISHED'}
            G.versions_list=r
            # (2)
            bpy.ops.ftrack.switch_panels(action="to versions")
            # (end)
            return{'FINISHED'}
        else:
            # (1)
            if G.role=="Working":
                b,r = working.open_version(task, int(self.action))
            else:
                b,r = working.open_version(task, int(self.action), look=True)
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            # (2)
            bpy.ops.ftrack.switch_panels(action="to working")
            # (end)
            self.report({'INFO'}, f"open version {self.action}")
            return{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        for line in G.message:
            col.label(text=line)

    def invoke(self, context, event):
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        G.message=list()
        dialog=False
        if self.action!='start' and G.lag:
            for item in CAUTION_OF_LAG:
                try:
                    G.message.append(item % G.lag)
                except:
                    G.message.append(item)
            dialog=True
        if self.action!='start' and task['status']['name'] in srvconn.CHEKING_STATUSES and G.role=="Working":
            G.message.append('-------')
            G.message=G.message+CAUTION_TO_INPROGRESS
            dialog=True

        if dialog:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

class FTRACK_commit(bpy.types.Operator):
    bl_idname = "ftrack.commit"
    bl_label = "Commit (PURGE!!!)"

    to_review: bpy.props.BoolProperty(name="To Review")
    description: bpy.props.StringProperty(name="Description")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        #(2)
        if self.to_review:
            bpy.ops.ftrack.check_scene('INVOKE_DEFAULT')
            if not G.check[0]:
                return{'FINISHED'}
        # (3)
        if self.to_review:
            status_name="Pending Review"
        else:
            status_name="In progress"
        b,v=working.commit(task, f"{self.description} [{bpy.app.version_string}]", status_name)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        b,r=srvconn.task_status_to(task, status_name)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        self.report({'INFO'}, f"Create version: {v}")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class FTRACK_download_version(bpy.types.Operator):
    bl_idname = "ftrack.download_version"
    bl_label = "Download Version"

    action: bpy.props.StringProperty(name="Action")

    def execute(self, context):
        # (1)
        if self.action=="start":
            b,r = get_versions_list()
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            bpy.ops.ftrack.switch_panels(action="to ftrack versions")
        else:
            # (1)
            task=G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
            b,r=working.download_version(task, G.ftrack_versions_dict[self.action])
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
            lag_test_of_version(self, task)
            self.report({'INFO'}, r)
            # (2)
            # bpy.ops.ftrack.update_incoming()
            # (3)
            # bpy.ops.ftrack.switch_panels(action="to working")
        # (end)
        return{'FINISHED'}

class FTRACK_update_incoming(bpy.types.Operator):
    bl_idname = "ftrack.update_incoming"
    bl_label = "Update incoming"

    def execute(self, context):
        # (1)
        b,r=srvconn.get_downloadable_incoming_links(for_download=True)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        G.components=r
        # return{'FINISHED'}
        bpy.ops.ftrack.download_incoming()
        self.report({'INFO'}, "Loading is complete!")
        return{'FINISHED'}

class FTRACK_download_incoming(bpy.types.Operator):
    bl_idname = "ftrack.download_incoming"
    bl_label = "Download?"

    info_data: bpy.props.StringProperty(name="Download data", default="qwerty")

    def execute(self, context):
        for task, vers, byte, url, status in G.components[0]:
            if vers and status in ('missing', 'old'):
                b,r=working.download_incoming_task_data_component(task, vers, url=url)
                if not b:
                    self.report({'WARNING'}, r)
                else:
                    self.report({'INFO'}, r)
            else:
                continue
        # self.report({'INFO'}, "Loading is complete!")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

class FTRACK_collecting_textures(bpy.types.Operator):
    bl_idname = "ftrack.collecting_textures"
    bl_label = "Collect textures"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b,r=working.collect_textures(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, "Textures collected")
        return{'FINISHED'}

class FTRACK_sources_panel(bpy.types.Operator):
    bl_idname = "ftrack.sources_panel"
    bl_label = "Sources panel"

    def execute(self, context):
        # (1)
        b,r=srvconn.get_downloadable_incoming_links()
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        G.components=r
        # (2)
        bpy.ops.ftrack.switch_panels(action="to sources")
        self.report({'INFO'}, "The sources panel is open")
        return{'FINISHED'}

class FTRACK_link_of_source(bpy.types.Operator):
    bl_idname = "ftrack.link_of_source"
    bl_label = "Incoming data"

    task_id: bpy.props.StringProperty(name="Task ID")
    # filepath: bpy.props.StringProperty(subtype="FILEPATH")

    def execute(self, context):
        #(1)
        self.report({'INFO'},G.sourcepath)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        #
        for item in G.components[0]:
            if item[0]["id"]==self.task_id:
                b,(r,path)=working.get_collections_of_source(item[0])
                if b:
                    G.sourcepath=path
                else:
                    r=[]
                break
        #
        layout = self.layout
        col = layout.column()
        col.label(text="List of collections:")
        for name in r:
            row = col.row(align = True)
            row.label(text=name)
            op=row.operator("ftrack.link")
            op.collection=name
            op.link=True
            op=row.operator("ftrack.link", text="Append")
            op.collection=name
            op.link=False

class FTRACK_link(bpy.types.Operator):
    bl_idname = "ftrack.link"
    bl_label = "Link"

    link: bpy.props.BoolProperty(name="Link", default=True)
    collection: bpy.props.StringProperty(name="Collection")

    def execute(self, context):
        # (1)
        b,r=working.link_collection(context, G.sourcepath, self.collection, self.link)
        self.report({'INFO'}, f"Linked {self.collection}")
        return{'FINISHED'}

class FTRACK_reload_incoming_version(bpy.types.Operator):
    bl_idname = "ftrack.reload_incoming_version"
    bl_label = "Reload version"

    task_id: bpy.props.StringProperty(name="Task ID")
    
    def execute(self, context):
        #(1)
        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}

    def invoke(self, context, event):
        get_versions_list(self.task_id)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text="List of versions:")
        for version in G.ftrack_versions_list:
            row = col.row(align = True)
            row.label(text=version['link'][-1:][0]['name'])
            row.label(text=version['comment'])
            row.label(text=str(version['date'].naive))
            op=row.operator("ftrack.reload_incoming_version_action")
            op.task_id=self.task_id
            op.version_id=version["id"]

class FTRACK_reload_incoming_version_action(bpy.types.Operator):
    bl_idname = "ftrack.reload_incoming_version_action"
    bl_label = "Download"

    task_id: bpy.props.StringProperty(name="Task ID")
    version_id: bpy.props.StringProperty(name="Version ID")

    def execute(self, context):
        #(1)
        b,r=working.download_incoming_task_data_component(self.task_id, G.ftrack_versions_dict[self.version_id])
        self.report({'INFO'}, f"task: {self.task_id} version:{self.version_id}")
        return{'FINISHED'}

class FTRACK_add_graphics_editors(bpy.types.Operator):
    bl_idname = "ftrack.add_graphics_editors"
    bl_label = "Add graphics editor"

    alias: bpy.props.StringProperty(name="Name")
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if not self.alias:
            self.report({'WARNING'}, "The name of the editor is required!")
            return{'FINISHED'}
        if self.filepath:
            settings.add_graphics_editors(self.alias, self.filepath)
            read_graphics_editors()

        self.report({'INFO'}, f"Added: {self.alias} - {self.filepath}")
        return{'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class FTRACK_del_graphics_editors(bpy.types.Operator):
    bl_idname = "ftrack.del_graphics_editors"
    bl_label = "Delete"

    alias: bpy.props.StringProperty()
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if self.alias:
            settings.del_graphics_editors(self.alias)
            read_graphics_editors()

        self.report({'INFO'}, f"Deleted: {self.alias}")
        return{'FINISHED'}

class FTRACK_open_graphics_editor(bpy.types.Operator):
    bl_idname = "ftrack.open_graphics_editor"
    bl_label = "Edit textures"

    app_item = [('-select application-',)*3]
    if not G.graphics_editors:
        read_graphics_editors()
    if G.graphics_editors:
        for key in G.graphics_editors:
            app_item.append((key,)*3)

    alias: bpy.props.EnumProperty(name = 'Open by:', items = tuple(app_item))
    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        if self.alias != "-select application-":
            working.open_images(G.graphics_editors[self.alias], self.filepath)

        self.report({'INFO'}, f"Opening: {self.alias}")
        return{'FINISHED'}

    def invoke(self, context, event):
        task=G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b, path = path_utils.get_folder_of_textures_path(task)
        if b:
            self.filepath = path + '/'
        # else:
        #     self.filepath = "//"
        wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class FTRACK_open_file_browser(bpy.types.Operator):
    bl_idname = "ftrack.open_file_browser"
    bl_label = "Open task folder"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b,r=working.open_task_folder_in_filebrowser(task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

class FTRACK_select_object(bpy.types.Operator):
    bl_idname = "ftrack.select_object"
    bl_label = "Select"

    ob_string: bpy.props.StringProperty() # from ob.__repr__()

    def execute(self, context):
        #(1)
        bpy.ops.object.select_all(action='DESELECT')
        #(2)
        ob=eval(self.ob_string)
        ob.select_set(True)
        context.view_layer.objects.active = ob
        
        self.report({'INFO'}, f"selected: {ob.name}")
        return{'FINISHED'}

class FTRACK_check_scene(bpy.types.Operator):
    bl_idname = "ftrack.check_scene"
    bl_label = "Check"

    def execute(self, context):
        if G.check and G.check[0]:
            self.report({'INFO'}, "All right!")
        return{'FINISHED'}

    def invoke(self, context, event):
        #(1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        #(2)
        b,r=check.check(task, context)
        G.check=(b,r)
        if not b:
            wm = context.window_manager
            return wm.invoke_props_dialog(self)
        else:
            return self.execute(context)

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        # col.label(text="Check list:")
        for key in G.check[1]:
            if not G.check[1][key]:
                continue
            col.label(text=f'{key}:')
            if isinstance(G.check[1][key], list):
                for item in G.check[1][key]:
                    row = col.row(align = True)
                    row.label(text=item)
                    row.operator("ftrack.select_object").ob_string=item
            else:            
                col.label(text=str(G.check[1][key]))

class FTRACK_change_status(bpy.types.Operator):
    bl_idname = "ftrack.change_status"
    bl_label = "Change status to \"Pending Review\""

    status_name: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b,r=srvconn.task_status_to(task, self.status_name)
        if not b:
            self.report({'WARNING'}, r)
            return{'FINISHED'}
        self.report({'INFO'}, f"status changed to {self.status_name}")
        return{'FINISHED'}


class FTRACK_change_status_of_selected_shot(bpy.types.Operator):
    bl_idname = "ftrack.change_status_of_selected_shot"
    bl_label = "Change review status"

    status_name: bpy.props.StringProperty()
    shot_name: bpy.props.StringProperty()

    def execute(self, context):
        b,r=working.change_status_of_selected_shot(self.shot_name, self.status_name)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, f"status changed to {self.status_name}")
        return{'FINISHED'}

class FTRACK_pack_links(bpy.types.Operator):
    bl_idname = "ftrack.pack_links"
    bl_label = "Pack Links"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        # (selection)
        if self.action=="selected":
            b,r=working.append_selected_link(context, context.object)
            if not b:
                self.report({'WARNING'}, r)
            else:
                self.report({'INFO'}, r)

        # self.report({'INFO'}, "pack_links")
        return{'FINISHED'}

class FTRACK_refresh_proxy(bpy.types.Operator):
    bl_idname = "ftrack.refresh_proxy"
    bl_label = "Refresh proxy"

    def execute(self, context):
        b,r=tools.refresh_proxy(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)

        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class FTRACK_download_animatic_to_shot(bpy.types.Operator):
    bl_idname = "ftrack.download_animatic_to_shot"
    bl_label = "Download animatic"

    # version: bpy.props.StringProperty(name='Version')

    def execute(self, context):
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b,r=working.download_animatic_to_shot(context, task)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)

        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class FTRACK_playblast(bpy.types.Operator):
    bl_idname = "ftrack.playblast"
    bl_label = "Local playblast"

    def execute(self, context):
        # (1)
        hooks._set_render_setting_for_playblast()
        bpy.ops.render.opengl(animation=True)
        self.report({'INFO'}, "playblast made")
        return{'FINISHED'}


class FTRACK_playblast_to_review(bpy.types.Operator):
    bl_idname = "ftrack.playblast_to_review"
    bl_label = "Playblast to version"

    to_review: bpy.props.BoolProperty(name="Status to Pending Review", default=True)
    use_last: bpy.props.BoolProperty(name="Use last playblast", default=False)
    description: bpy.props.StringProperty(name="Description")
    commit: bpy.props.BoolProperty(name="Make commit", default=True)

    def execute(self, context):
        #(0) playblast
        if not self.use_last:
            hooks._set_render_setting_for_playblast(fstart='ENV', fend='ENV')
            bpy.ops.render.opengl(animation=True)
        #(1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        #(2) new status
        if self.to_review:
            status_name="Pending Review"
        else:
            status_name="In progress"
        #(3) to review
        path=bpy.path.abspath(bpy.context.scene.render.filepath)
        if not os.path.exists(path):
            self.report({'WARNING'}, "Video file not found!")
            return{'FINISHED'}
        b,v=working.playblast_to_review(task, self.description, path, status_name)
        if not b:
            self.report({'WARNING'}, v)
            return{'FINISHED'}
        #(4) change status
        if task['status']['name'] != status_name:
            b,r=srvconn.task_status_to(task, status_name)
            if not b:
                self.report({'WARNING'}, r)
                return{'FINISHED'}
        self.report({'INFO'}, f"Playblast to version: {v}")
        if self.commit:
            bpy.ops.ftrack.commit(description=self.description, to_review=self.to_review)
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        # wm = context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class FTRACK_download_animatic(bpy.types.Operator):
    bl_idname = "ftrack.download_animatic"
    bl_label = "Download animatic"

    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        b,r=working.download_animatic(context, task, self.filepath)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class FTRACK_rename_animatic_markers(bpy.types.Operator):
    bl_idname = "ftrack.rename_animatic_markers"
    bl_label = "Rename markers"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.action=='sequence':
            b,r=working.rename_sequences_markers(context)
        elif self.action=='shot':
            b,r=working.rename_shots_markers(context)
        else:
            self.report({'WARNING'}, 'Unknown action!')
            return{'FINISHED'}
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class FTRACK_create_shots_from_markers(bpy.types.Operator):
    bl_idname = "ftrack.create_shots_from_markers"
    bl_label = "Create Shots from markers"

    step: bpy.props.IntProperty(name='Num of shots', default=10)
    template: bpy.props.EnumProperty(items=fill_task_templates, name = 'Task templates', default=None, update = None)

    def execute(self, context):
        # (1)
        b,r=working.create_shots_from_markers(context, step=self.step, template=self.template)
        if not b:
            self.report({'WARNING'}, f"create_shots_from_markers: \"{r}\"")
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        get_task_templates()
        # wm = context.window_manager
        # return wm.invoke_props_dialog(self)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

    # def draw(self, context):
    #     layout = self.layout
    #     col = layout.column()
    #     col.label(text="List of versions:")


class FTRACK_re_create_selected_shots(bpy.types.Operator):
    bl_idname = "ftrack.re_create_selected_shots"
    bl_label = "Re-create selected Animatic"

    def execute(self, context):
        # (1)
        b,r=working.re_create_selected_shots(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class FTRACK_download_shot_animatic_to_episode(bpy.types.Operator):
    bl_idname = "ftrack.download_shot_animatic_to_episode"
    bl_label = "Refresh selected Animatic"

    def execute(self, context):
        # (1)
        b,r=working.download_shot_animatic_to_episode(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class FTRACK_download_review(bpy.types.Operator):
    bl_idname = "ftrack.download_review"
    bl_label = "Download last Review"

    def execute(self, context):
        # (1)
        b,r=working.download_review(context)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class FTRACK_select_shot_sequences_by_task_status(bpy.types.Operator):
    bl_idname = "ftrack.select_shot_sequences_by_task_status"
    bl_label = "Select shot sequences by task status"

    # status: bpy.props.StringProperty(name="Task status", default="Pending Review")
    statuses=[
        ("In progress",)*3,
        ("Pending Review",)*3,
        ("Needs attention",)*3,
        ("Proposed final",)*3,
        ("Approved",)*3,
    ]
    status: bpy.props.EnumProperty(name = 'Task status', items = statuses)

    def execute(self, context):
        # (1)
        b,r=working.select_shot_sequences_by_task_status(context, self.status)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, f"Selected shot sequences by status {self.status}")
        return{'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class FTRACK_light_local_save_to_file(bpy.types.Operator):
    bl_idname = "ftrack.light_local_save_collections"
    bl_label = "Save to local text file"

    def execute(self, context):
        # (1)
        b,r=lcm.save_collections_children(lcm.COLLECTIONS)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}

    def invoke(self, context, event):
        get_task_templates()
        # wm = context.window_manager
        # return wm.invoke_props_dialog(self)
        wm = context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}


class FTRACK_light_set_from_file(bpy.types.Operator):
    bl_idname = "ftrack.light_set_from_file_collections"
    bl_label = "Set from local text file"

    action: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.action=='local':
            b,r=lcm.set_collections_children(context)
            if not b:
                self.report({'WARNING'}, r)
            else:
                self.report({'INFO'}, "Ok!")
        else:
            b,r=srvconn.get_downloadable_incoming_links(task_id=os.environ["FTRACK_B3D_CURRENT_TASK_ID"], for_download=True)
            if not b:
                self.report({'WARNING'}, r)
            else:
                components=r[0]
                for item in components:
                    if item[0]["parent"]["type"]["name"]=="Location":
                        b,r=lcm.set_collections_children(context, incoming_task=item[0])
                        if not b:
                            self.report({'WARNING'}, r)
                        else:
                            self.report({'INFO'}, f'Set data from - {item[0]["parent"]["type"]["name"]} - {item[0]["parent"]["name"]} - {item[4]}')
                self.report({'INFO'}, "Ok!")
        return{'FINISHED'}


class FTRACK_light_select_objects_of_collections(bpy.types.Operator):
    bl_idname = "ftrack.light_select_objects_of_collections"
    bl_label = "Select objects"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        if self.collection=="No":
            lcm.select_no_collections_objects()
        else:
            lcm.select_objects_of_collection(self.collection)
        # if not b:
        #     self.report({'WARNING'}, r)
        # else:
        #     self.report({'INFO'}, r)
        return{'FINISHED'}


class FTRACK_light_add_selected_objects_to_collections(bpy.types.Operator):
    bl_idname = "ftrack.light_add_selected_objects_to_collections"
    bl_label = "Add selected"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        b,r=lcm.add_selected_objects_to_collection(context, self.collection)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class FTRACK_light_remove_selected_objects_from_collections(bpy.types.Operator):
    bl_idname = "ftrack.light_remove_selected_objects_from_collections"
    bl_label = "Remove selected"

    collection: bpy.props.StringProperty()

    def execute(self, context):
        # (1)
        b,r=lcm.remove_selected_objects_from_collection(context, self.collection)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class FTRACK_open_last_version_by_webbrowser(bpy.types.Operator):
    bl_idname = "ftrack.open_last_version_by_webbrowser"
    bl_label = "Open last Version by web"

    prefix: bpy.props.StringProperty(default="Review_")
    shot_name: bpy.props.StringProperty(default="")

    def execute(self, context):
        # (1)
        if self.shot_name:
            b,r=working.open_last_version_by_webbrowser(self.prefix, asset_name=self.shot_name)
        else:
            b,r=working.open_last_version_by_webbrowser(self.prefix)
        if not b:
            self.report({'WARNING'}, r)
        else:
            self.report({'INFO'}, r)
        return{'FINISHED'}


class FTRACK_timing_from_selected_shots(bpy.types.Operator):
    bl_idname = "ftrack.timing_from_selected_shots"
    bl_label = "Timing from selected shots"

    def execute(self, context):
        # (1)
        b,r=working.timing_from_selected_shots(context)

        self.report({'INFO'}, "Ok!")
        return{'FINISHED'}


class FTRACK_backup(bpy.types.Operator):
    bl_idname = "ftrack.backup"
    bl_label = "Backup"

    path: bpy.props.StringProperty()
    limit: bpy.props.IntProperty(default=0)

    def execute(self, context):
        # (1)
        os.environ["FTRACK_B3D_BACKUP_ROOT"]=self.path
        # (2)
        b,r=working.backup(self.path, G.projects_dict[os.environ["FTRACK_B3D_CURRENT_PROJECT_ID"]], limit=self.limit)
        if not b:
            self.report({'WARNING'}, str(r))
        else:
            self.report({'INFO'}, str(r))
        return{'FINISHED'}


### TEMPLATE
class FTRACK_template_operator(bpy.types.Operator):
    bl_idname = "ftrack.template_operator"
    bl_label = "Text"

    def execute(self, context):
        # (1)
        task = G.task_dict[os.environ["FTRACK_B3D_CURRENT_TASK_ID"]]
        self.report({'INFO'}, "template_operator")
        return{'FINISHED'}
        
def register():
    # bpy.utils.register_class(FTRACK_main_panel)
    bpy.utils.register_class(FTRACK_VIEW_3D_panel)
    bpy.utils.register_class(FTRACK_SEQUENCE_EDITOR_panel)
    bpy.utils.register_class(FTRACK_auth_start)
    bpy.utils.register_class(FTRACK_setting_start)
    bpy.utils.register_class(FTRACK_authorization)
    bpy.utils.register_class(FTRACK_manual)
    bpy.utils.register_class(FTRACK_api)
    bpy.utils.register_class(FTRACK_set_folder)
    bpy.utils.register_class(FTRACK_select_task)
    bpy.utils.register_class(FTRACK_current_scene_to_work)
    bpy.utils.register_class(FTRACK_open_scene_from_incoming_task)
    bpy.utils.register_class(FTRACK_switch_panels)
    bpy.utils.register_class(FTRACK_open_work_top_version)
    bpy.utils.register_class(FTRACK_open_work_version)
    bpy.utils.register_class(FTRACK_commit)
    bpy.utils.register_class(FTRACK_download_version)
    bpy.utils.register_class(FTRACK_update_incoming)
    bpy.utils.register_class(FTRACK_download_incoming)
    bpy.utils.register_class(FTRACK_collecting_textures)
    bpy.utils.register_class(FTRACK_sources_panel)
    bpy.utils.register_class(FTRACK_link_of_source)
    bpy.utils.register_class(FTRACK_link)
    bpy.utils.register_class(FTRACK_add_graphics_editors)
    bpy.utils.register_class(FTRACK_del_graphics_editors)
    bpy.utils.register_class(FTRACK_open_graphics_editor)
    bpy.utils.register_class(FTRACK_open_file_browser)
    bpy.utils.register_class(FTRACK_reload_incoming_version)
    bpy.utils.register_class(FTRACK_reload_incoming_version_action)
    bpy.utils.register_class(FTRACK_select_object)
    bpy.utils.register_class(FTRACK_check_scene)
    bpy.utils.register_class(FTRACK_change_status)
    bpy.utils.register_class(FTRACK_change_status_of_selected_shot)
    bpy.utils.register_class(FTRACK_pack_links)
    bpy.utils.register_class(FTRACK_playblast)
    bpy.utils.register_class(FTRACK_playblast_to_review)
    bpy.utils.register_class(FTRACK_refresh_proxy)
    bpy.utils.register_class(FTRACK_download_animatic)
    bpy.utils.register_class(FTRACK_download_animatic_to_shot)
    bpy.utils.register_class(FTRACK_rename_animatic_markers)
    bpy.utils.register_class(FTRACK_create_shots_from_markers)
    bpy.utils.register_class(FTRACK_re_create_selected_shots)
    bpy.utils.register_class(FTRACK_download_shot_animatic_to_episode)
    bpy.utils.register_class(FTRACK_download_review)
    bpy.utils.register_class(FTRACK_select_shot_sequences_by_task_status)
    bpy.utils.register_class(FTRACK_light_panel)
    bpy.utils.register_class(FTRACK_light_local_save_to_file)
    bpy.utils.register_class(FTRACK_light_set_from_file)
    bpy.utils.register_class(FTRACK_light_select_objects_of_collections)
    bpy.utils.register_class(FTRACK_light_add_selected_objects_to_collections)
    bpy.utils.register_class(FTRACK_light_remove_selected_objects_from_collections)
    # bpy.utils.register_class(FTRACK_light_set_from_incoming_task)
    bpy.utils.register_class(FTRACK_open_last_version_by_webbrowser)
    bpy.utils.register_class(FTRACK_timing_from_selected_shots)
    bpy.utils.register_class(FTRACK_backup)
    set_params()

def unregister():
    # bpy.utils.unregister_class(FTRACK_main_panel)
    bpy.utils.unregister_class(FTRACK_VIEW_3D_panel)
    bpy.utils.unregister_class(FTRACK_SEQUENCE_EDITOR_panel)
    bpy.utils.unregister_class(FTRACK_auth_start)
    bpy.utils.unregister_class(FTRACK_setting_start)
    bpy.utils.unregister_class(FTRACK_authorization)
    bpy.utils.unregister_class(FTRACK_manual)
    bpy.utils.unregister_class(FTRACK_api)
    bpy.utils.unregister_class(FTRACK_set_folder)
    bpy.utils.unregister_class(FTRACK_select_task)
    bpy.utils.unregister_class(FTRACK_current_scene_to_work)
    bpy.utils.unregister_class(FTRACK_open_scene_from_incoming_task)
    bpy.utils.unregister_class(FTRACK_switch_panels)
    bpy.utils.unregister_class(FTRACK_open_work_top_version)
    bpy.utils.unregister_class(FTRACK_open_work_version)
    bpy.utils.unregister_class(FTRACK_commit)
    bpy.utils.unregister_class(FTRACK_download_version)
    bpy.utils.unregister_class(FTRACK_update_incoming)
    bpy.utils.unregister_class(FTRACK_download_incoming)
    bpy.utils.unregister_class(FTRACK_collecting_textures)
    bpy.utils.unregister_class(FTRACK_sources_panel)
    bpy.utils.unregister_class(FTRACK_link_of_source)
    bpy.utils.unregister_class(FTRACK_link)
    bpy.utils.unregister_class(FTRACK_add_graphics_editors)
    bpy.utils.unregister_class(FTRACK_del_graphics_editors)
    bpy.utils.unregister_class(FTRACK_open_graphics_editor)
    bpy.utils.unregister_class(FTRACK_open_file_browser)
    bpy.utils.unregister_class(FTRACK_reload_incoming_version)
    bpy.utils.unregister_class(FTRACK_reload_incoming_version_action)
    bpy.utils.unregister_class(FTRACK_select_object)
    bpy.utils.unregister_class(FTRACK_check_scene)
    bpy.utils.unregister_class(FTRACK_change_status)
    bpy.utils.unregister_class(FTRACK_change_status_of_selected_shot)
    bpy.utils.unregister_class(FTRACK_pack_links)
    bpy.utils.unregister_class(FTRACK_playblast)
    bpy.utils.unregister_class(FTRACK_playblast_to_review)
    bpy.utils.unregister_class(FTRACK_refresh_proxy)
    bpy.utils.unregister_class(FTRACK_download_animatic)
    bpy.utils.unregister_class(FTRACK_download_animatic_to_shot)
    bpy.utils.unregister_class(FTRACK_rename_animatic_markers)
    bpy.utils.unregister_class(FTRACK_create_shots_from_markers)
    bpy.utils.unregister_class(FTRACK_re_create_selected_shots)
    bpy.utils.unregister_class(FTRACK_download_shot_animatic_to_episode)
    bpy.utils.unregister_class(FTRACK_download_review)
    bpy.utils.unregister_class(FTRACK_select_shot_sequences_by_task_status)
    bpy.utils.unregister_class(FTRACK_light_panel)
    bpy.utils.unregister_class(FTRACK_light_local_save_to_file)
    bpy.utils.unregister_class(FTRACK_light_set_from_file)
    bpy.utils.unregister_class(FTRACK_light_select_objects_of_collections)
    bpy.utils.unregister_class(FTRACK_light_add_selected_objects_to_collections)
    bpy.utils.unregister_class(FTRACK_light_remove_selected_objects_from_collections)
    # bpy.utils.unregister_class(FTRACK_light_set_from_incoming_task)
    bpy.utils.unregister_class(FTRACK_open_last_version_by_webbrowser)
    bpy.utils.unregister_class(FTRACK_timing_from_selected_shots)
    bpy.utils.unregister_class(FTRACK_backup)
    srvconn.timer_stop()

def auth(context=bpy.context):
    r,m=srvconn.auth(
        context.scene.ftrack_server_url,
        context.scene.ftrack_api_user,
        context.scene.ftrack_api_key,
        )
    context.scene.ftrack_auth_in_process=False
    if r:
        os.environ["FTRACK_B3D_AUTH_USER"]=context.scene.ftrack_auth_current_user=context.scene.ftrack_api_user
        # get projects
        b,r=srvconn.get_active_projects_list(sess=m)
        if b:
            G.projects=r[0]
            G.projects_dict=r[1]
            # print(G.projects)
            # for key in G.projects_dict.keys():
            #     print(G.projects_dict[key]["custom_attributes"]["fps"])

        return(True, 'Successful Authentication')
    else:
        os.environ["FTRACK_B3D_AUTH_USER"]=context.scene.ftrack_auth_current_user='No authorization'
        return(False, m)