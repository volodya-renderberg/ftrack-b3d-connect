# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack

import logging

import ftrack_api.entity.factory

TASKS_READABLE_FIELDS =[
    "name",
    "id",
    "project_id",
    "link",
    "type.name",
    "status.name",
    "parent.id",
    "parent.name",
    "parent.object_type.name",
    "parent.custom_attributes"
    ]
"""list: Считываемые поля задачи. """

PROJECTS_READABLE_FIELDS =[
    "name",
    "full_name",
    "id",
    "custom_attributes"
    ]
"""list: Считываемые поля проекта. """

VERSIONS_READABLE_FIELDS=[
    "task.link",
    "task_id",
    "link",
    "id",
    "comment",
    "date",
    "user.last_name"
    ]
"""list: Считываемые поля версии. """


class Factory(ftrack_api.entity.factory.StandardFactory):
    '''Entity class factory.'''

    def create(self, schema, bases=None):
        '''Create and return entity class from *schema*.'''
        # Optionally change bases for class to be generated.
        cls = super(Factory, self).create(schema, bases=bases)

        # Further customise cls before returning.
        if schema['id'] == 'Task':
            cls.default_projections = TASKS_READABLE_FIELDS
        elif schema['id'] == 'AssetVersion':
            cls.default_projections = VERSIONS_READABLE_FIELDS
        elif schema['id'] == 'Project':
            cls.default_projections = PROJECTS_READABLE_FIELDS

        return cls


def register(session):
    '''Register plugin with *session*.'''
    logger = logging.getLogger('ftrack_plugin:construct_entity_type.register')

    # Validate that session is an instance of ftrack_api.Session. If not, assume
    # that register is being called from an old or incompatible API and return
    # without doing anything.
    if not isinstance(session, ftrack_api.Session):
        logger.debug(
            'Not subscribing plugin as passed argument {0!r} is not an '
            'ftrack_api.Session instance.'.format(session)
        )
        return

    factory = Factory()

    def construct_entity_type(event):
        '''Return class to represent entity type specified by *event*.'''
        schema = event['data']['schema']
        return factory.create(schema)

    session.event_hub.subscribe(
        'topic=ftrack.api.session.construct-entity-type',
        construct_entity_type
    )
