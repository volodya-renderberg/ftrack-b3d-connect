# -*- coding: utf-8 -*-

"""Поиск путей задач, проектов, таблиц. """

import os
import json
import logging
import traceback

try:
    import settings
except:
    from . import settings

PATH_IDENTIFIER = 'id'
"""str: Ключ из ``task[link]`` значение которого используется для построения путей директорий. """

BACKUP_PATH_IDENTIFIER = 'name'
"""str: Ключ из ``task[link]`` значение которого используется для построения путей директорий при бекапе. """

BACKUP_FOLDER_DATA = 'data.json'
"""str: имя файла содержащего основные параметры директори. 

.. code-block:: python

    {
        "type":'',
        "object_type":'',
        "name":'',
        "id":''
    }
"""

BACKUP_VERSIONS_FOLDER='versions'
"""str: имя директории с ``beckup`` версиями задачи. """

PROJECT_DB = '.project_data.db'
"""str: Имя файла базы данных sqlite где хранятся данные загруженных версий задач ассетов. Расположение в папке проекта. """

VERSIONS_DB = '.versions_data.db'
"""str: Имя файла базы данных sqlite где хранятся данные локальных версий текущей задачи. Расположение в папке задачи. """

EXT='.blend'
"""str: Расширение для рабочих файлов по умолчанию. """

TEXTURES_FOLDER = "textures"
"""str: Название директории текстур задачи. """

PREFIX_ANIMATIC='Animatic_'
"""str: Префикс видео файлов аниматика. """


def _remove_temp_file(path):
    """Удаляет файл и его директорию. Предполагается, что директория создавалась процедурой tempfile.mkdtemp() """
    os.remove(path)
    try:
        os.rmdir(os.path.dirname(path))
    except:
        pass


def _task_id_from_path(path) -> str:
    """Возвращает **id** задачи (или None) по ``path`` - путь к файлу или папке, любой вложенности (макс глубина 20) в директории задачи. 

    * Например мувка аниматика или плейбласта.
    * Также вернёт **id** если путь сама директория задачи.

    Returns
    -------
    str
        **id** задачи
    """
    dir=settings.PROJECTS
    parts=list()
    i=0
    def cp(path, i):
        if i>20:
            return
        #(1)
        item=os.path.basename(path)
        if item==dir:
            if i>1:
                return parts[-2:][0]
            else:
                return
        parts.append(item)
        i+=1
        return cp(os.path.dirname(path), i)
        
    
    return cp(path, i)


def _create_path(path: str, create: bool) -> tuple:
    """Создание при необходимости директории до path. """
    if create and not os.path.exists(path):
        try:
            os.makedirs(path)
            return(True, path)
        except Exception as e:
            print(f'problem in _create_path({path}): {e}')
            print(f'{traceback.format_exc()}')
            return (False, f"{e}")
    else:
        return(True, path)

def get_task_folder_path(task, create=True, backup=False) -> tuple:
    """Возвращает локальный путь до директории задачи, при необходимости создаёт.

    .. note:: путь создаётся по ``task[link].item[id]``

    Parameters
    ----------
    task : Task
        Данная задача.
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.
    backup : bool
        Если True - то кроме директории создаёт в ней :attr:`BACKUP_FOLDER_DATA` файл с необходимыми данными.

    Returns
    -------
    tuple
        (True, path_to_task_folder) или (False, comment)

    """
    print(f'INFO: get folder path of {task["parent"]["name"]}/{task["name"]}')
    if not task:
        return(False, f"In get_task_folder_path() task={task}")
    if backup:
        projects_folder=os.environ.get('FTRACK_B3D_BACKUP_ROOT')
    else:
        projects_folder=os.environ.get('FTRACK_B3D_PROJECTS_DIR')
    
    if not projects_folder or not os.path.exists(projects_folder):
        return (False, f"In get_task_folder_path() Projects Dir is not defined or does not exist: {projects_folder}")

    path=projects_folder
    # (var 1)
    if backup:
        for item in task["link"]:
            path = os.path.join(path, item[BACKUP_PATH_IDENTIFIER])
            if create:
                _create_path(path, create)
                if item['type']=="TypedContext":
                    if not os.path.exists(os.path.join(path,BACKUP_FOLDER_DATA)):
                        ob=task.session.query(f'select custom_attributes, type.name, object_type.name, status.name from TypedContext where id={item["id"]}')[0]
                        ob_status=ob_type=ob_object_type=None
                        if ob.get('type'):
                            ob_type=ob['type']['name']
                        if ob.get('object_type'):
                            ob_object_type=ob['object_type']['name']
                        if ob.get('status'):
                            ob_status=ob['status']['name']
                        data={
                            "type":ob_type,
                            "object_type":ob_object_type,
                            "name":item['name'],
                            "id":item["id"],
                            "custom_attributes": dict(ob['custom_attributes']),
                            "status": ob_status
                            }
                        with open(os.path.join(path,BACKUP_FOLDER_DATA), 'w') as f:
                            json.dump(data, f, indent=4, sort_keys=True)


    # (var 2)
    else:
        path=os.path.join(path, task["link"][0][PATH_IDENTIFIER])
        path=os.path.join(path, task["link"][-1:][0][PATH_IDENTIFIER])

    return _create_path(path, create)


def get_project_folder_path(task, create=True) -> tuple:
    """Возвращает локальный путь до директории проекта, при необходимости создаёт.

    .. note:: путь создаётся по ``task[link].item[id]``

    Parameters
    ----------
    task : Task
        Данная задача.
    create : bool, optional
        Если **True**, то при отсутствии директория будет создана.

    Returns
    -------
    tuple
        (True, path) или (False, comment)

    """
    projects_folder=os.environ.get('FTRACK_B3D_PROJECTS_DIR')

    if not projects_folder or not os.path.exists(projects_folder):
        return (False, f"Projects Dir is not defined or does not exist: {projects_folder}")

    path = os.path.join(projects_folder, task["link"][0][PATH_IDENTIFIER])

    return _create_path(path, create)

def get_folder_of_textures_path(task, create=True) -> tuple:
    """Возвращает путь до директории textures задачи, при необходимости создаёт. """
    b,r=get_task_folder_path(task)
    if not b:
        return(b,r)
    path = os.path.join(r, TEXTURES_FOLDER)
    return _create_path(path, create)

def get_project_db_path(task) -> tuple:
    """Путь до файла :attr:`PROJECT_DB` """

    # projects_dir = os.environ.get('FTRACK_B3D_PROJECTS_DIR')
    # if not projects_dir or not os.path.exists(projects_dir):
    #     return (False, f'"Projects Dir" ({projects_dir}) - not defined or does not exist, make settings of "Projects Dir"')
    # project_path=os.path.join(projects_dir, project_id)
    # if not os.path.exists(project_path):
    #     return (False, f'Directory of project: ({project_path}) - does not exist')

    b, project_path=get_project_folder_path(task)
    if not b:
        return(False, project_path)

    path = os.path.join(project_path, PROJECT_DB)
    return (True, path)

def get_versions_db_path(task) -> tuple:
    """Путь до файла :attr:`VERSIONS_DB` """

    b, r = get_task_folder_path(task)
    if not b:
        return(False, r)
    path = os.path.join(r, VERSIONS_DB)
    return (True, path)

def get_top_version_path_of_workfile(task) -> tuple:
    """Возвращает путь до локальной ``top`` версии рабочего файла.

    .. note:: Название файла - ``task[parent][name].ext``.
        
    Parameters
    ----------
    task : Task
        Данная задача.
    
    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b, r = get_task_folder_path(task)
    if not b:
        return(b, r)
    #() get ext
    ext=EXT
    path=os.path.join(r, f'{task["parent"]["name"]}{ext}')
    return(True, path)

def get_version_path_of_workfile(task, version) -> tuple:
    """Возвращает путь до локальной версии рабочего файла.

    .. note:: расположение в ``task_folder/versions/``, нейминг ``asset_name__v0000.ext``.
        
    Parameters
    ----------
    task : Task
        Данная задача.
    version : str, int
        Номер версии.

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b, r = get_task_folder_path(task)
    if not b:
        return(b, r)
    #() get ext
    ext=EXT
    path=os.path.join(r, 'versions', f'{task["parent"]["name"]}_v{version}{ext}')
    return(True, path)


def _backup_update_folder_data(folder_path, update_data):
    """Дописывание или переписывание данных в data.json директории

    Parameters
    ----------
    folder_path : str
        Путь директории
    update_data : dict
        Новые значения

    Returns
    -------
    bool
        True в случае удачи, или None
    """
    data_path=os.path.join(folder_path, BACKUP_FOLDER_DATA)
    logging.debug(data_path)
    if os.path.exists(data_path):
        logging.debug("exists")
        with open(data_path, "r") as f:
            logging.debug("open")
            data=json.load(f)

        logging.debug(data)
        try:
            if isinstance(data, dict) and isinstance(update_data, dict):
                data.update(update_data)
            elif isinstance(data, list) and isinstance(update_data, list):
                data=data+update_data
            else:
                raise Exception(e)
        except Exception as e:
            logging.error(f'{e}\n{update_data}')
            logging.error(f'{traceback.format_exc()}')
            raise Exception(e)

        with open(data_path, "w") as f:
            json.dump(data, f, indent=4, sort_keys=True)
        return True
    else:
        with open(data_path, "w") as f:
            json.dump(update_data, f, indent=4, sort_keys=True)
        return True


def _backup_get_folder_data(folder_path: str, default=dict()):
    """Чтение данных в data.json директории

    Parameters
    ----------
    folder_path : str
        Путь директории
    
    Returns
    -------
    dict или None
    """
    data_path=os.path.join(folder_path, BACKUP_FOLDER_DATA)
    if os.path.exists(data_path):
        with open(data_path, "r") as f:
            data=json.load(f)
        return data
    else:
        return default  